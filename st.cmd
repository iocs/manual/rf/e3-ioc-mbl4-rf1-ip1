require gammaspc
require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("P", "MBL-040RFC:")
epicsEnvSet("R", "RFS-VacPS-110:")
epicsEnvSet("IP", "mbl4-rf1-ip1.tn.esss.lu.se:23")
epicsEnvSet("PORT", "GAMMA")

iocshLoad "$(gammaspc_DIR)/gammaSpce.iocsh"

